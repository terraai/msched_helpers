# Generated from instructions below
# http://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_cloudwatch_logs.html#installing_cwl_agent
#
# Make sure jq is installed
# sudo yum install -y jq
import os

# Copy new file
#
# Fix !!!!!!!! -> copy from dir to file
# Make into function and call this?
#
os.system('sudo mv /etc/awslogs/awslogs.conf /etc/awslogs/awslogs.conf.bak')

# Replace cluster
os.system('''cluster=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .Cluster')''')
os.system('sudo sed -i -e "s/{cluster}/$cluster/g" /etc/awslogs/awslogs.conf')

# Replace instance id
os.system('''container_instance_id=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F/ '{print $2}' )''')
os.system('sudo sed -i -e "s/{container_instance_id}/$container_instance_id/g" /etc/awslogs/awslogs.conf')

# Copy new file
#
# Fix !!!!!!!! -> copy from dir to file
# Make into function and call this?
os.system('sudo mv /etc/awslogs/awslogs.conf /etc/awslogs/awslogs.conf.bak')

# Start the service
os.system('sudo service awslogs start')
os.system('sudo chkconfig awslogs on')
