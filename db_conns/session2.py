from sqlalchemy import DDL


# https://stackoverflow.com/questions/13677781/getting-sqlalchemy-to-issue-create-schema-on-create-all
event.listen(Base.metadata, 'before_create', DDL("CREATE SCHEMA IF NOT EXISTS my_schema"))



