
import urllib2
import os

def start_integrations(home = '/root'):
    # Make dir
    os.system('mkdir '+home+'/.aws')

    try:
        # Pull dir
        response = urllib2.urlopen('http://msched.us-west-2.elasticbeanstalk.com/credentials')
        infile = response.read()
        os.chdir(home+'/.aws')
        with open('credentials', 'w') as outfile:
            outfile.write(infile)
    except urllib2.URLError:
        print 'Could not load credentials'

    # -----------
    # Download the jars
    # file.retrieve("")
    # response = urllib2.urlopen('http://msched.us-west-2.elasticbeanstalk.com/')
    # infile = response.read()
    os.system('aws s3 cp s3://webservice-deploy/ModelSchedulerClient-0.1.jar ' + home + '/ModelSchedulerClient-0.1.jar')

    # -----------cron
    # Download the heart_beat crontab
    try:
        response = urllib2.urlopen('http://msched.us-west-2.elasticbeanstalk.com/crontab')
        infile = response.read()
        dir = '/etc/cron.d/'
        os.chdir(dir)
        with open('heart_beat', 'w') as outfile:
            outfile.write(infile)
    except urllib2.URLError:
        print 'Could not load crontab'

    # Start cron
    os.system('/etc/init.d/cron start')
    print 'crond'
