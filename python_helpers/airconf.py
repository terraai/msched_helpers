
import os
import urllib

def get_af_conf(af_profile = 'testing', home = '/root', source = '/ModelSchedulerConfig/content/'):
    # Pull airflow.cfg
    if af_profile != 'testing':
        afc = urllib.URLopener()
        afc.retrieve('http://msched.us-west-2.elasticbeanstalk.com/AirflowConfig', '$AIRFLOW_HOME/airflow.cfg')
    else:
        # --------------- TEMP
        #  move airflow.cfg to /airflow
        os.system('rm /airflow/airflow.cfg')
        # Move the config to the AIRFLOW_HOME
        os.system('mv ' + home + source + 'airflow.cfg $AIRFLOW_HOME')

def af_link_dags_dir(home = '/root', dir = '/dags'):
    # Symbolic link to dags folder
    # if os.path.exists('/')
    os.system('ln -s '+home + dir + ' $AIRFLOW_HOME/dags')

def af_start_websched(port=8080):
    os.system('airflow initdb')
    # AIRFLOW scheduler and Webserver
    os.system('airflow scheduler & airflow webserver -p '+str(port))

def af_start_worker(queue = 'testing'):
    os.system('airflow worker -q ' + queue)

def write_airflow_cfg():
    """
    Airflow.cfg.
        core
            dags_folder
            s3_log_folder
            executor
            parallelism
            dag_concurrency
        cli
            api_client
            endpoint_url
            auth_backend
        webserver
    """
    print 'here'