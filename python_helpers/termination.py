import os
import time

from datetime import datetime

from instance import get_describe_instances
from instance import get_instance_id

def get_termination_policy():
    print 'here'

def get_launch_time():
    instance_id=get_instance_id()
    if instance_id != 'testing':
        response = get_describe_instances(instance_id)
        launch_time = response['Reservations'][0]['Instances'][0]['LaunchTime']
        return launch_time
    else:
        return 'testing'

def time_to_billing_sec():
    launch_time = get_launch_time()
    n = datetime.now()
    simple_dif_sec = (launch_time.minute*60 + launch_time.second) - (n.minute*60 + n.second)
    if simple_dif_sec < 0:
        return 3600 + simple_dif_sec
    else:
        return simple_dif_sec

def time_min_to_billing(launch_time=get_launch_time()):
    l = launch_time
    n = datetime.now()
    print type(n)
    # billing_time = datetime.datetime(
    #     n.year,
    #     n.month,
    #     n.day,
    #     n.hour,
    #     l.minute,
    #     l.second
    # ) + datetime.timedelta(seconds = time_to_billing_sec(l))
    billing_time = n + datetime.timedelta(seconds = time_to_billing_sec(l))
    return billing_time


def modify_shutdown_behavior(home_dir):
    # Change rc.shutdown to only shutdown in desired window polled from ec2
    # Could get really hairy
    print 'here'

def start_waiting_termination(poll_sec=5, hour_add_wait=0):
    tp = round((time_to_billing_sec()+hour_add_wait*3600)/ poll_sec)
    for t in range(1, int(tp)):
        # There could be a little improvement here as I think there are still a few poll trips till termination
        print 'Waiting ' + str(poll_sec*(int(tp) - t)) + ' seconds till termination polling every ' + str(poll_sec) + ' seconds'
        time.sleep(poll_sec)
    os.system('sudo shutdown')