import os
import urllib2

def get_git(home = '/root'):
    # -----------ssh
    # Download the git credentials and add them to ssh-agent

    if not os.path.exists(home + '/.ssh'):
        # This is sort of redundent as we assume ssh is installed in which case this dir exists
        os.system('mkdir ' + home + '/.ssh')

    os.chdir(home + '/.ssh')

    for p in ['prv', 'pub']:
        response = urllib2.urlopen('http://msched.us-west-2.elasticbeanstalk.com/'+p+'_git_key')
        infile = response.read()

        with open(p+'_git_key', 'w') as outfile:
            outfile.write(infile)
    # Rename the files ---- Could be id_rsa....
    os.system('mv '+home+'/.ssh/pub_git_key '+home+'/.ssh/git_gen.pub')
    os.system('mv '+home+'/.ssh/prv_git_key '+home+'/.ssh/git_gen')



    # Add ssh keys

    bash_cmd="""
    eval `ssh-agent -s`;
    chmod 400 """+home+"""/.ssh/git_gen;
    ssh-add """+home+"""/.ssh/git_gen;
    cat > """+home+"""/.ssh/config<<-EOM
    Host bitbucket.org
     IdentityFile """+home+"""/.ssh/git_gen
    """
    os.system(bash_cmd)
    os.system('chmod 600 '+home+'/.ssh/config;')

    # Change git config to pull from repo
    git_config = """rm """+home+"""/.ssh/config;
    cat > """+home+"""/.git/config<<-EOM
    [core]
            repositoryformatversion = 0
            filemode = false
            bare = false
            logallrefupdates = true
            symlinks = false
            ignorecase = true
    [remote "origin"]
            url = git@bitbucket.org:terraai/msched.git
            fetch = +refs/heads/*:refs/remotes/origin/*
    [branch "master"]
            remote = origin
            merge = refs/heads/master
    """

    os.system('ssh-add '+home+'/.ssh/git_gen')
    os.system(git_config)

    # -----------git
    # Sync repos
    os.system('cd '+home)
    os.system('git pull git@bitbucket.org:terraai/msched.git/')

