import boto3
import httplib2
import datetime
import time
import os

def get_instance_id():
    # Poll aws for instance id
    try:
        conn = httplib2.Http(".cache", timeout=1)
        response, instance_id = conn.request("http://169.254.169.254/latest/meta-data/instance-id")
        if response.status==200:
            return instance_id
        else:
            return 'testing'
    except:
        print "We are not in AWS"
        return 'testing'

def get_describe_instances():
    # Gives all info on instance
    instance_id=get_instance_id()
    ec2 = boto3.client('ec2', region_name='us-west-2')
    if instance_id != 'testing':
        response = ec2.describe_instances(InstanceIds=[instance_id])
        return response
    else:
        return 'testing'

def get_instance_tags():
    # Returns list of kv pairs
    instance_id=get_instance_id()
    if instance_id != 'testing':
        tags = get_describe_instances()
        tags = tags['Reservations'][0]['Instances'][0]['Tags']
        return tags
    else:
        return 'testing'

def check_tag(ctag):
    # Checks arbitrary key in tags and returns value
    instance_id = get_instance_id()
    if instance_id != 'testing':
        tags = get_instance_tags()
        for k in tags:
            if k['Key']==ctag:
                return k['Value']
    else:
        return 'testing'


