
from collections import OrderedDict
import json


def read_config(dir=None, config_name=None):

    if dir is None:
        dir = '~/config.json'
    if config_name is None:
        config_name = 'config.json'

    path = dir + config_name

    with open(path, 'r') as conf:
        cfg = OrderedDict(json.load(conf, object_pairs_hook=OrderedDict))

    return cfg


