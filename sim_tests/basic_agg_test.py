from random import random
import time
import os

def testing_agg(max_realizations, sim_dir, agg_dir, sim_names=[], agg_name="testing.txt", sim_size=1024):
    # Checks if all the simulations are in place to perform an aggregation
    for r in range(1, max_realizations):
        if not os.path.isdir(sim_dir+sim_names[r-1]):
            print "Path doesn't exist"
        else:
            file_size = os.stat(sim_dir + sim_names[r-1])
            print 'sim ' + sim_names[r-1] + 'exists and is ' + file_size.st_size/1024 + ' kb large'

    if not os.path.isdir(agg_dir):
        print 'Making agg_dir'
        os.mkdir(agg_dir)

    wait = random()

    time.sleep(wait)
    print "waited " + str(wait)

    # Write test file
    try:
        with open(agg_dir + agg_name, "wb") as out:
            out.truncate(sim_size)
    except:

        print "already exists " + agg_dir + agg_name
        pass

