#!/bin/bash
yum install -y aws-cli
yum update -y
yum -y install nfs-utils
mkdir -p /home/ec2-user/efs
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).fs-536cb2fa.efs.us-west-2.amazonaws.com:/ /home/ec2-user/efs
echo ECS_CLUSTER=staging >> /etc/ecs/ecs.config
#service docker restart && start ecs