# Basic
ansible
celery
airflow
apache-airflow[crypto,celery,postgres,bash]
psycopg2
pyyaml
cfn_flip
Cython
pytz
pyOpenSSL
ndg-httpsclient
pyasn1
#