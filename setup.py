from setuptools import setup

setup(name='fdo_python_helpers',
      version='0.01',
      description='terra.ai fdo helpers (obj model, dags, connectors, build helpers)',
      url='https://bitbucket.org/terraai/fdo_python_helpers/src',
      author='rob + reza',
      author_email='rob@terra.ai',
      license='terra.ai',
      packages=['build_helpers','objm','dags', 'state_control', 'connectors'],
	  include_package_data=True,
      zip_safe=False)